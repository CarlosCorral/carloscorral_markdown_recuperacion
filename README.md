# Hoja03 Markdown ej 02



### A
 Crea un repositorio en GitLab o GitHub llamado TUNOMBRE_markdown
 ![Imagen](Capturas/Captura.PNG)
### B
 Clona el repositorio en local  
 ![Imagen](Capturas/Captura2.PNG)
### C 
Crea en tu repositorio local un documento README.md
Nota: en este documento tendrás que ir poniendo los comandos que has tenido que
utilizar durante todos los ejercicios y las explicaciones y capturas de pantalla que consideres necesarias  
 ![Imagen](Capturas/Captura3.PNG)

### D
 Añadir al README.md los comandos utilizados hasta ahora y hacer un commit inicial con el mensaje “Primer commit de TUNOMBRE”.  
  ![Imagen](Capturas/Captura4.PNG)
 ![Imagen](Capturas/Captura5.PNG)
 ![Imagen](Capturas/Captura6.PNG)

### E
 Sube los cambios al repositorio remoto.
![Imagen](Capturas/Captura7.PNG)
![Imagen](Capturas/Captura8.PNG)

### F 
Crear en el repositorio local un fichero llamado privado.txt. Crear en el repositorio
local una carpeta llamada privada.

![Imagen](Capturas/Captura9.PNG)
### G
 Realizar los cambios oportunos para que tanto el archivo como la carpeta sean
ignorados por git.
Creo un archivo con el nombre ".gitignore" (nombre necesario) y escribo los archivos que deseo que sean ignorados
  ![Imagen](Capturas/Captura10.PNG)
### H 
Documenta los puntos e) f) y g) en el fichero README.md  
  ![Imagen](Capturas/Captura11.PNG)
### I
 Añade el fichero tunombre.md en el que se muestre un listado de los módulos en los que estás matriculado.
  ![Imagen](Capturas/Captura12.PNG)
### J 
Crea un tag llamado v0.1
  ![Imagen](Capturas/Captura13.PNG)
### K 
Sube los cambios al repositorio remoto
  ![Imagen](Capturas/Captura14.PNG)
   ![Imagen](Capturas/Captura15.PNG)
### L
 Documenta los puntos i) j) y k) en el README.md
  ![Imagen](Capturas/Captura16.PNG)
### M 
Por último, crea una tabla en el documento anterior en el que se muestre el nombre
de 2 compañeros y su enlace al repositorio en GitLab o GitHub.

Nombre|Enlace (falso)
--|--
Alberto|[Enlace 1](http://www.educacion.cantabria.es )
Juan Carlos|[Enlace 2](http://www.google.es)

***

# Hoja03 Markdown ej 03

A partir de la tarea anterior vamos a profundizar más en el uso de Git:
### 1.- Creación de ramas:
#### a) Crea la rama rama-TUNOMBRE
![Imagen](Capturas/Captura17.PNG)
#### b) Posiciona tu carpeta de trabajo en esta rama
![Imagen](Capturas/Captura18.PNG)
### 2.- Añade un fichero y crea la rama remota
#### c) Crea un fichero llamado despligue.md con únicamente una cabecera DESPLIEGUE DE APLICACIONES WEB
![Imagen](Capturas/Captura19.PNG)
#### d) Haz un commit con el mensaje “Añadiendo el archivo despliegue.md en la ramaTUNOMBRE”
![Imagen](Capturas/Captura20.PNG)
#### e) Sube los cambios al repositorio remoto. NOTA: date cuenta que ahora se deberá hacer con el comando git push origin rama-TUNOMBRE
![Imagen](Capturas/Captura21.PNG)
### 3.- Haz un merge directo
#### f) Posiciónate en la rama master
![Imagen](Capturas/Captura22.PNG)
#### g) Haz un merge de la rama-TUNOMBRE en la rama master
![Imagen](Capturas/Captura23.PNG)
### 4.- Haz un merge con conflicto
#### h) En la rama master añade al fichero despliegue.md una tabla en la que muestres los temas de la primera evaluación de Despliegue de Aplicaciones Web
![Imagen](Capturas/Captura26.PNG)
#### i) Añade los archivos y haz un commit con el mensaje “Añadida primera evaluación Despliegue”
![Imagen](Capturas/Captura24.PNG)
#### j) Posiciónate ahora en la rama-TUNOMBRE
![Imagen](Capturas/Captura25.PNG)
#### k) Escribe en el fichero despliegue.md otra tabla con los temas de la segunda evaluación de Despliegue de Aplicaciones Web
![Imagen](Capturas/Captura27.PNG)
#### l) Añade los archivos y haz un commit con el mensaje “Añadida tabla segunda evaluación Despliegue”
![Imagen](Capturas/Captura28.PNG)
#### m) Posiciónate otra vez en master y haz un merge con la rama-TUNOMBRE
![Imagen](Capturas/Captura29.PNG)
### 5.- Arreglo del conflicto
#### n) Arregla el conflicto editando el fichero despliegue.md y haz un commit con el mensaje “Finalizado el conflicto de despliegue.md”
![Imagen](Capturas/Captura30.PNG)
![Imagen](Capturas/Captura31.PNG)
### 6.- Tag y borrar la rama
#### o) Crea un tag llamado v0.2
![Imagen](Capturas/Captura32.PNG)
#### p) Borra la rama-TUNOMBRE
![Imagen](Capturas/Captura33.PNG)
### 7.- Documenta todo y finaliza el ejercicio
#### q) En el fichero README.md crea una nueva sección en la que vayas documentando todo lo que vas realizando en esta tarea.
![Imagen](Capturas/Captura37.PNG)
#### r) Documenta todos los puntos en el README.md, haz un commit y sube los cambios al servidor
![Imagen](Capturas/Captura34.PNG)
#### s) Haz un último commit y sube todo al servidor
![Imagen](Capturas/Captura35.PNG)


